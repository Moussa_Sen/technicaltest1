package com.tech.technicaltest.runtime.resources;

import com.tech.technicaltest.domain.dtos.DepositDTO;
import com.tech.technicaltest.domain.services.DepositService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value="/api/v1/deposit")
public class DepositResources {

    private final DepositService depositService;

    public DepositResources(DepositService depositService) {
        this.depositService = depositService;
    }

    @Operation(summary = "add new deposit ")
    @PostMapping("")
    public DepositDTO saveDeposit (@RequestBody @Valid DepositDTO depositDTO)  {
        return this.depositService.saveDeposit(depositDTO);
    }

    @Operation(summary = "retrieve all deposits")
    @GetMapping("")
    public List<DepositDTO> listDeposits()  {
        return this.depositService.DepositDTOList();
    }

    @Operation(summary = "get deposit by ref")
    @GetMapping("/{depositId}")
    public DepositDTO getDeposit(@PathVariable Long depositId) {
        return this.depositService.getDeposit(depositId);
    }

}
