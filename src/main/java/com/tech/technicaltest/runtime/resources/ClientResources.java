package com.tech.technicaltest.runtime.resources;

import com.tech.technicaltest.domain.dtos.ClientDTO;
import com.tech.technicaltest.domain.services.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;


@RestController
@RequestMapping(value="/api/v1/client")
public class ClientResources {

    private final ClientService clientService;

    public ClientResources(ClientService clientService) {
        this.clientService = clientService;
    }

    @Operation(summary = "add new client ")
    @PostMapping("")
    public ClientDTO saveClient (@RequestBody @Valid ClientDTO clientDTO)  {
        return this.clientService.saveClient(clientDTO);
    }

    @Operation(summary = "retrieve all clients")
    @GetMapping("")
    public List<ClientDTO> listClients()  {
        return this.clientService.ClientDTOList();
    }

    @Operation(summary = "get client by ref")
    @GetMapping("/{clientId}")
    public ClientDTO getClient(@PathVariable @Min(1) Integer clientId) {
        return this.clientService.getClient(clientId);
    }

}
