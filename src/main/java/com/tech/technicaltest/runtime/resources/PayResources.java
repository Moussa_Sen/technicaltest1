package com.tech.technicaltest.runtime.resources;

import com.tech.technicaltest.domain.dtos.SoldeDTO;
import com.tech.technicaltest.domain.services.PayService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value="/api/v1/pay")
public class PayResources {

    private final PayService payService;

    public PayResources(PayService payService) {
        this.payService = payService;
    }

    @Operation(summary = "calculer la facture d'un client")
    @PostMapping("/calculate")
    public double calculerEnergie(@RequestBody SoldeDTO cons) {
        return this.payService.calculate(cons.clientId());
    }

}
