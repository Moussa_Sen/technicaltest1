package com.tech.technicaltest.runtime.resources;

import com.tech.technicaltest.domain.dtos.CompanyDTO;
import com.tech.technicaltest.domain.services.CompanyService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value="/api/v1/company")
public class CompanyResources {

    private final CompanyService companyService;

    public CompanyResources(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Operation(summary = "add new company ")
    @PostMapping("")
    public CompanyDTO saveCompany (@RequestBody @Valid CompanyDTO companyDTO)  {
        return this.companyService.saveCompany(companyDTO);
    }

    @Operation(summary = "retrieve all compagnies")
    @GetMapping("")
    public List<CompanyDTO> listCompanys()  {
        return this.companyService.CompanyDTOList();
    }

    @Operation(summary = "get company by ref")
    @GetMapping("/{companyId}")
    public CompanyDTO getCompany(@PathVariable Long companyId) {
        return this.companyService.getCompany(companyId);
    }

}
