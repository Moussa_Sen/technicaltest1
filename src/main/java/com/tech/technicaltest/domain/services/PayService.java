package com.tech.technicaltest.domain.services;

import com.tech.technicaltest.domain.entities.Client;
import com.tech.technicaltest.domain.entities.Deposit;
import com.tech.technicaltest.runtime.handlers.errors.ResourceException;
import com.tech.technicaltest.runtime.repository.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

import static com.tech.technicaltest.domain.utils.ConvertDate.getFebDate;
import static com.tech.technicaltest.domain.utils.ConvertDate.isDifferenceGreaterThan365;

@Service
@Slf4j
@Transactional

public class PayService {

    private final ClientRepository clientRepository;

    public PayService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }
    public double calculate(Integer clientId ) {

        Optional <Client> client = clientRepository.findById(clientId);

        if(!client.isPresent())
            throw new ResourceException(HttpStatus.NOT_FOUND,"Client not found");

        return getDeposit(client.get());
    }
    public double getDeposit(Client client ) {
        double mysolde=0;

        LocalDate dateNow = LocalDate.now();
        List<Deposit> deposits = client.getDeposits();
        if(deposits==null)
            throw new ResourceException(HttpStatus.NOT_FOUND,client.getFullname() + " does not have deposits");

        for (Deposit d:deposits) {

            var startDateFeb = getFebDate(d.getStartDate());

            if(d.getTypeDeposit().name().equals("GIFT") && !isDifferenceGreaterThan365(d.getStartDate(), dateNow)) {
                mysolde += d.getAmount();
            }
            if(d.getTypeDeposit().name().equals("MEAL") && dateNow.isBefore(startDateFeb)  ) {
                mysolde += d.getAmount();
            }
        }
        return mysolde;

    }

}
