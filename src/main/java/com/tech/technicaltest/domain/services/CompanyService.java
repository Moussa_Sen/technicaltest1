package com.tech.technicaltest.domain.services;

import com.tech.technicaltest.domain.dtos.CompanyDTO;
import com.tech.technicaltest.domain.entities.Company;
import com.tech.technicaltest.domain.mappers.MyMapper;
import com.tech.technicaltest.runtime.handlers.errors.ResourceException;
import com.tech.technicaltest.runtime.repository.CompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
//@AllArgsConstructor

public class CompanyService {

    private final CompanyRepository companyRepository;

    private final MyMapper myMapper;
    
    public CompanyService(CompanyRepository companyRepository, MyMapper myMapper) {
        this.companyRepository = companyRepository;
        this.myMapper=myMapper;
    }

    public  List<CompanyDTO> CompanyDTOList() {

        List<Company> compagnies = this.companyRepository.findAll();

        return compagnies.stream()
                .map(myMapper::toCompanyDto)
                .collect(Collectors.toList());

    }

    public CompanyDTO getCompany(Long compagnieId) {
        Company company =this.companyRepository.findById(compagnieId)
                .orElseThrow(()->new ResourceException(HttpStatus.NOT_FOUND,"Company not found"));

        return this.myMapper.toCompanyDto(company);
    }

    public CompanyDTO saveCompany(CompanyDTO companyDTO) {
        Company company = new Company();
        company.setName(companyDTO.name());
        company.setBalance(companyDTO.balance());

        return this.myMapper.toCompanyDto(this.companyRepository.save(company));

    }
    
}
