package com.tech.technicaltest.domain.services;

import com.tech.technicaltest.domain.dtos.DepositDTO;
import com.tech.technicaltest.domain.entities.Client;
import com.tech.technicaltest.domain.entities.Company;
import com.tech.technicaltest.domain.entities.Deposit;
import com.tech.technicaltest.domain.mappers.MyMapper;
import com.tech.technicaltest.runtime.handlers.errors.ResourceException;
import com.tech.technicaltest.runtime.repository.ClientRepository;
import com.tech.technicaltest.runtime.repository.CompanyRepository;
import com.tech.technicaltest.runtime.repository.DepositRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
//@AllArgsConstructor

public class DepositService {

    private final DepositRepository depositRepository;
    private final ClientRepository clientRepository;
    private final CompanyRepository companyRepository;

    private final MyMapper myMapper;

    public DepositService(DepositRepository depositRepository, ClientRepository clientRepository, CompanyRepository companyRepository, MyMapper myMapper) {
        this.depositRepository = depositRepository;
        this.clientRepository = clientRepository;
        this.companyRepository = companyRepository;
        this.myMapper=myMapper;
    }

    public  List<DepositDTO> DepositDTOList() {

        List<Deposit> deposits = this.depositRepository.findAll();

        return deposits.stream()
                .map(myMapper::toDepositDto)
                .collect(Collectors.toList());

    }

    public DepositDTO getDeposit(Long depositId) {
        Deposit client=this.depositRepository.findById(depositId)
                .orElseThrow(()->new ResourceException(HttpStatus.NOT_FOUND,"Deposit not found"));

        return this.myMapper.toDepositDto(client);
    }

    public DepositDTO saveDeposit(DepositDTO depositDTO) {

        Client client = this.clientRepository.findById(depositDTO.getClientId()).orElseThrow(()->new ResourceException(HttpStatus.NOT_FOUND,"Client not found"));
        Company company = this.companyRepository.findById(depositDTO.getCompanyId()).orElseThrow(()->new ResourceException(HttpStatus.NOT_FOUND,"Company not found"));

        double soldeDeposit =client.getDeposits().stream()
                .filter(f-> Objects.equals(f.getCompany().getId(), depositDTO.getCompanyId()))
                .map(Deposit::getAmount)
                .reduce(0.0, Double::sum) + depositDTO.getAmount();

        if(company.getBalance()<depositDTO.getAmount() || company.getBalance()<soldeDeposit)
            throw new ResourceException(HttpStatus.BAD_REQUEST,"Company balance is insuffisant");

        Deposit deposit = new Deposit();
        deposit.setTypeDeposit(depositDTO.getTypeDeposit());
        deposit.setAmount(depositDTO.getAmount());
        deposit.setStartDate(depositDTO.getStartDate());
        deposit.setCompany(company);
        deposit.setClient(client);

        return this.myMapper.toDepositDto(this.depositRepository.save(deposit));
    }

}
