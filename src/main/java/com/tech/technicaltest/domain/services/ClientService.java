package com.tech.technicaltest.domain.services;

import com.tech.technicaltest.domain.dtos.ClientDTO;
import com.tech.technicaltest.domain.entities.Client;
import com.tech.technicaltest.domain.mappers.MyMapper;
import com.tech.technicaltest.runtime.handlers.errors.ResourceException;
import com.tech.technicaltest.runtime.repository.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
//@AllArgsConstructor

public class ClientService {

    private final ClientRepository clientRepository;

    private final MyMapper myMapper;

    public ClientService(ClientRepository clientRepository,MyMapper myMapper) {
        this.clientRepository = clientRepository;
        this.myMapper=myMapper;
    }

    public  List<ClientDTO> ClientDTOList() {

        List<Client> clients = this.clientRepository.findAll();

        return clients.stream()
                .map(myMapper::toClientDto)
                .collect(Collectors.toList());

    }

    public ClientDTO getClient(Integer clientId) {
        Optional<Client> client=this.clientRepository.findById(clientId);
        if(!client.isPresent())
                throw new ResourceException(HttpStatus.NOT_FOUND,"Client not found");

        return this.myMapper.toClientDto(client.get());
    }

    public ClientDTO saveClient(ClientDTO clientDTO) {
        Client client = new Client();
        client.setFullname(clientDTO.fullname());

        return this.myMapper.toClientDto(this.clientRepository.save(client));

    }

}
