package com.tech.technicaltest.domain.utils;


import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class ConvertDate {

    public static boolean isDifferenceGreaterThan365(Date startDate, LocalDate now){

        return ChronoUnit.DAYS.between(convertToLocalDate(startDate), now) > 364;
    }

    public static LocalDate convertToLocalDate(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static LocalDate getFebDate(Date startDate) {
        var year =  Integer.parseInt(new SimpleDateFormat("yyyy").format(startDate));
        Calendar cal = Calendar.getInstance();
        cal.set(year + 1, Calendar.FEBRUARY,Calendar.DAY_OF_MONTH);

        var startDateFeb = LocalDate.of(year + 1, 2, cal.getActualMaximum(Calendar.DATE));

        return startDateFeb;
    }
}
