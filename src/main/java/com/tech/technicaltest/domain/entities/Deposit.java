package com.tech.technicaltest.domain.entities;


import com.tech.technicaltest.domain.enums.TypeDeposit;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
public class Deposit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double amount;
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Enumerated(EnumType.STRING)
    private TypeDeposit typeDeposit;
//    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @ManyToOne()
    private Company company;
    @ManyToOne()
    private Client client;

}
