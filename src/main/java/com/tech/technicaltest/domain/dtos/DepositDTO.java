package com.tech.technicaltest.domain.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tech.technicaltest.domain.enums.TypeDeposit;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import java.util.Date;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
public class DepositDTO {

    private Long id;
//    @Min(1)
    private double amount;
//    @NotBlank
    private TypeDeposit typeDeposit;
    @Temporal(TemporalType.DATE)
    @JsonProperty("start_date")
    private Date startDate;
    @Min(1)
    private Long companyId;
    @Min(1)
    private Integer clientId;
}
