package com.tech.technicaltest.domain.dtos;

import lombok.Builder;

@Builder
public record CompanyDTO(Long id,String name,double balance) {
}
