package com.tech.technicaltest.domain.mappers;

import com.tech.technicaltest.domain.dtos.ClientDTO;
import com.tech.technicaltest.domain.dtos.CompanyDTO;
import com.tech.technicaltest.domain.dtos.DepositDTO;
import com.tech.technicaltest.domain.entities.Client;
import com.tech.technicaltest.domain.entities.Company;
import com.tech.technicaltest.domain.entities.Deposit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface MyMapper {

    ClientDTO toClientDto(final Client client);

    @Mapping(target = "companyId", source = "company.id")
    @Mapping(target = "clientId", source = "client.id")
    DepositDTO toDepositDto(final Deposit deposit);

    CompanyDTO toCompanyDto(final Company company);

}
