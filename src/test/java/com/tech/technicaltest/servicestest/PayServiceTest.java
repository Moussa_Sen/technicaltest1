package com.tech.technicaltest.servicestest;

import com.tech.technicaltest.domain.dtos.ClientDTO;
import com.tech.technicaltest.domain.dtos.CompanyDTO;
import com.tech.technicaltest.domain.dtos.DepositDTO;
import com.tech.technicaltest.domain.services.ClientService;
import com.tech.technicaltest.domain.services.CompanyService;
import com.tech.technicaltest.domain.services.PayService;
import com.tech.technicaltest.runtime.handlers.errors.ResourceException;
import com.tech.technicaltest.domain.enums.TypeDeposit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import java.sql.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=RANDOM_PORT)
public class PayServiceTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PayService payService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private CompanyService companyService;

    @Test
    public void should_returnSolde(){
        assertThat(payService.calculate(1)).isEqualTo(130);
    }

    @Test(expected = ResourceException.class)
    public void should_returnException(){
        assertThat(clientService.getClient(5)).isEqualTo(ClientDTO.builder().id(1).fullname("Matilde").build());

    }
    public void should_getClient(){
        assertThat(clientService.getClient(1)).isEqualTo(ClientDTO.builder().id(1).fullname("Bernard").build());

    }
    @Test
    public void should_getCompany(){
        assertThat(companyService.getCompany(1L))
                .isEqualTo(
                        CompanyDTO.builder().id(1L).name("Apple").balance(1500).build());
    }
    @Test
    public void should_getClientSize(){
        assertThat(clientService.ClientDTOList().size()).isEqualTo(2);

    }
    @Test
    public void should_getCompanySize(){
        assertThat(companyService.CompanyDTOList().size()).isEqualTo(3);
    }

    @Test
    public void should_createClient() {

        ClientDTO cli = ClientDTO.builder()
                .id(8)
                .fullname("Sarah Raush")
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        HttpEntity<ClientDTO> request = new HttpEntity<>(cli, headers);

        ResponseEntity<ClientDTO> response =
                restTemplate.postForEntity("/api/v1/client",request ,ClientDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().fullname()).isEqualTo("Sarah Raush");
    }

    @Test
    public void should_createDeposit() {

        DepositDTO depositDTO = DepositDTO.builder()
                .amount(380)
                .typeDeposit(TypeDeposit.MEAL)
                .clientId(1)
                .companyId(1L)
                .startDate(Date.valueOf("2022-09-25"))
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        HttpEntity<DepositDTO> request = new HttpEntity<>(depositDTO, headers);

        ResponseEntity<DepositDTO> response =
                restTemplate.postForEntity("/api/v1/deposit",request ,DepositDTO.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getAmount()).isEqualTo(380);
    }

}
